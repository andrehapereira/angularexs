var myApp = angular.module('myApp',[]);
var apiKey = "d9266cdd33d69d1147fd52adedaa36a6";
var myChart;

myApp.controller('RequestController', function($scope, $http) {
  angular.element(document).ready(function () {
        $('.vert-center').css('top', $(window).height() / 2 - $('.vert-center').height() / 2);
        $scope.toggle('metric');
    });
    angular.element(window).resize(function () {
      $('.vert-center').css('top', $(window).height() / 2 - $('.vert-center').height() / 2);
    });

  $scope.countries = [{name:"Lisbon, PT", id : "6458923"},
  {name : "London, GB", id : "6058560"},
  {name : "Rio de Janeiro, BR", id : "3451190"},
  {name : "New York, US", id : "5128638"},
  {name : "Paris, FR", id : "4717560"}];

  $scope.nextHourForecast = [];

  $scope.units = [{name : "ºC", type : "metric"}, {name : "ºF", type : "imperial"}];
  $scope.selectedCity = "6458923";
  $scope.selectedUnit = $scope.units[0].type;

  $scope.showSelectedCity = function(selectedCity, selectedUnit) {
    $scope.selectedUnit = selectedUnit;
    //REQUEST WEATHER
    $http.get("http://api.openweathermap.org/data/2.5/weather?id=" + selectedCity + "&appid=" + apiKey + "&units=" + selectedUnit)
    .then(function(response) {
        $scope.cityData = response.data;
    })
    //END REQUEST WEATHER
    //REQUEST FORECAST
    $http.get("http://api.openweathermap.org/data/2.5/forecast?id=" + selectedCity + "&appid=" + apiKey + "&units=" + selectedUnit)
    .then(function(response) {
        $scope.cityForecast = response.data;
        var todayDate = new Date();
        var hours = 10;
        $scope.nextHourForecast = [];
        response.data.list.forEach(function (element) {
          var elementDate = new Date(element.dt_txt);
          if(todayDate < elementDate && hours >= 1) {
            $scope.nextHourForecast.push(element);
            hours--;
          }
        })
        $scope.chartInit();
    })
    //END REQUEST FORECAST
  };

  $scope.toggle = function(selectedUnit) {
    if(selectedUnit === 'metric') {
      $('.metric').addClass('active');
      $('.imperial').removeClass('active');
    } else {
      $('.imperial').addClass('active');
      $('.metric').removeClass('active');
    }
  }

  //Chart
  $scope.chartInit = function() {
    if(myChart !== undefined) {
      myChart.destroy();
    }
    var ctx = document.getElementById("myChart").getContext('2d');
    var times = [];
    var temperature = [];
    var windspeed = [];
    var bgcolor = [];
    var bordercolor = [];
    $scope.nextHourForecast.forEach(function (hour) {
      times.push(new Date(hour.dt_txt).toString().split(" ")[0] + " - " + hour.dt_txt.split(" ")[1].substring(0,5));
      temperature.push(hour.main.temp);
      windspeed.push(hour.wind.speed);
      bgcolor.push('rgba(9,50,100, 0.1)');
      bordercolor.push('rgba(9,50,100, 0.5)');
    });
    myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: times,
            datasets: [{
                label: 'Temperature',
                data: temperature,
                backgroundColor:bgcolor,
                borderColor: bordercolor,
                borderWidth: 2
            }, {
              label: 'Wind Speed',
              data: windspeed,
              type: 'line',
              backgroundColor: [
                  'rgba(255, 206, 86,0)'
              ],
              borderColor: [
                  'rgba(255, 206, 86, 1)'
              ],
              borderWidth: 2
            }]
        },

        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        callback: function(value, index, values) {
                          var tempType = 'Fº - m/h';
                          if($scope.selectedUnit == 'metric') {
                            tempType = 'Cº - m/s'
                          }
                          return value + " " + tempType;
                        },
                        fontColor: 'rgba(9,50,100, 1)',
                        fontStyle: 'bold'
                    },
                    gridLines: {
                      display:false,
                      zeroLineWidth: 2,
                      zeroLineColor: 'rgba(9,50,100, 1)'
                    }
                }],
                xAxes: [{
                  ticks: {
                    fontColor: 'rgba(9,50,100, 1)',
                    fontStyle: 'bold'
                  },
                  gridLines: {
                      display:true,
                    zeroLineWidth: 2,
                    zeroLineColor: 'rgba(9,50,100, 1)'
                  }
                }]
            }
        }
    });
  }
  //END CHAR
});
