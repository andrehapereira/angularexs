## Exercise 1

1. We will use Open Weather Data to create a single page application that presents a list of 5 European cities (you can choose the ones you prefer).
2. Your goal is to get the current weather situation displaying the city name plus average temperature and the wind strength.
3. Clicking on an item shows the forecast in the next hours.
   You can adjust the UI how you see fit for the best result, but sticking to a single page application is mandatory.

---

## Exercise 2.3

1. We will use Open Weather Data to create a single page application that presents a list of 5 European cities (you can choose the ones you prefer).
2. Your goal is to get the current weather situation displaying the city name plus average temperature and the wind strength.
3. Clicking on an item shows the temperature in the next 5 days at 12:00.
   You can adjust the UI how you see fit for the best result, but sticking to a single page application is mandatory.

---

## Exercise 2.7

1. We will use http://ergast.com/mrd/ to create a single page application that presents a list that shows the F1 world champions starting from 2005 until 2015.
2. Clicking on an item shows the list of the winners for every race for the selected year.
3. We also request to highlight the row when the winner has been the world champion in the same season.
4. You can adjust the UI how you see fit for the best result, but sticking to a single page application is mandatory. 

## Constraints

1. Please don’t use ES6
2. It is ok to use dependency management tools and task runners, but please make sure your exercise runs simply by opening your main HTML file in a browser.
3. Please make sure the delivered package contains an unminified and human readable version of your sources.
4. Please refrain from leveraging scaffolding tools (like yeoman) or boilerplate templates.
5. Please don’t use out-of-the-box plugins.
6. Please create a readme file that explains what you have done

---
