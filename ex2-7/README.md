## Coding Summary

Everything happens in a single page. The main page contains a dropdown with the seasons (2005-2015 as requested. 2005 as default). Selecting a season displays:

1. Number of races for that season.
2. Number of drivers for that season.
3. Season champion.
4. Team of the Season Champion.

Clicking on Round Details button, a table drops down with information about each race.

Clicking on the info button of a particular race opens a modal in which we're able to see a table with the race results, driver position, etc, etc.

As well as the season winner highlighted in yellow with a little trophy too.

Clicking on the info icon button drops down a table with information from each season (between 2005 and 2015) that particular driver participated about final positions, and teams representing.

Note that before you're able to interact with each Race Results, an hover is shown showing the loading process of the information. Info icon buttons are red while info is not loaded.

---
