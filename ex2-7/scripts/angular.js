var myApp = angular.module('myApp',[]);
var myChart;
myApp.controller('RequestController', function($scope, $http) {
  angular.element(document).ready(function () {
    $('.vert-center').css('top', '5%');
    $('.loading').css('padding-top', $(window).height() / 2.2 + 'px');
  });
  angular.element(window).resize(function () {
    $('.loading').css('padding-top', $(window).height() / 2.2 + 'px');
  });

  $scope.seasons = ["2005", "2006","2007","2008","2009","2010","2011","2012","2013","2014","2015"];
  $scope.selectedSeason = "2005";
  $scope.arrowAngle = "down";
  $scope.changeAngle = function() {
    if($scope.arrowAngle === "down") {
      $scope.arrowAngle = "up";
    } else {
      $scope.arrowAngle = "down";
    }
  }
  $scope.requestSeason = function(season) {

    //REQUEST SEASON
    $http.get("http://ergast.com/api/f1/" + season + ".json")
    .then(function(response) {
      $scope.seasonDetails = response.data.MRData;
      $scope.seasonRaces = $scope.seasonDetails.RaceTable.Races;
    })
    //END REQUEST SEASON
    //REQUEST SEASON DRIVERS
    $http.get("http://ergast.com/api/f1/" + season + "/drivers.json")
    .then(function(response) {
      $scope.seasonDrivers = response.data.MRData;
    })
    //END REQUEST SEASON DRIVERS
    //REQUEST SEASON STANDINGS
    $http.get("http://ergast.com/api/f1/" + season + "/driverStandings.json")
    .then(function(response) {
      $scope.seasonDriverStandings = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
      $scope.seasonWinner = $scope.seasonDriverStandings[0];
    })
    //END REQUEST SEASON WINNER
  };

});

myApp.controller('DriverController', function($http, $parse) {
  var vm = this;
  vm.seasons = ["2005", "2006","2007","2008","2009","2010","2011","2012","2013","2014","2015"];
  vm.requestRaceResults = function(season, round) {
    $http.get("http://ergast.com/api/f1/" + season +"/" + round + "/results.json")
    .then(function(response) {
      vm.raceDetails = response.data.MRData.RaceTable.Races[0];
      vm.raceResults = vm.raceDetails.Results;
      vm.driverStandings = [];
      vm.requestSeasonsStandings();
      vm.btnCollIcon = "fa-ban";
      vm.btnClass = "btn-danger";
    })
  };
  vm.requestSeasonsStandings = function() {
    loadStart();
    vm.loading = 0;
    var loadingLength = vm.raceResults.length * vm.seasons.length;
    var counter = 0;
    vm.seasons.forEach(function(season) {
      var auxObj = {};
      auxObj.drivers = [];
      vm.raceResults.forEach(function(result) {
        $http.get("http://ergast.com/api/f1/" + season + "/drivers/" + result.Driver.driverId + "/driverStandings.json")
        .then(function(response) {
          var data = response.data.MRData.StandingsTable.StandingsLists[0];
          if(data !== undefined){
            vm.driverStandings.push(data);
            vm.driverStandings.sort(compare);
          }
          counter++;
          vm.loading = (100 * counter) / loadingLength;
          if(counter === loadingLength) {
            loadFinish();
            enableButtons();
            vm.btnCollIcon = "fa-info-circle";
            vm.btnClass = "btn-primary";
          }
        })
      });
    });
  };

  vm.requestDriverInfo = function(driverId) {
    vm.driverDetails = [];
    vm.driverStandings.forEach(function(standing) {
      vm.obj = {}
      if(standing.DriverStandings[0].Driver.driverId === driverId) {
        vm.obj.season = standing.season
        vm.obj.driver = standing.DriverStandings[0];
        vm.driverDetails.push(vm.obj);
      }
    });
  }

  vm.collapseClick = function() {
    $('.driver-info.collapse').collapse('hide');
  };
});


var $accordionGrp = $('.accordion-grp');
$accordionGrp.on('show','.collapse', function() {
  $accordionGrp.find('.collapse.in').collapse('hide');
});

function compare(season, nextSeason) {
  if(season.season < nextSeason.season) {
    return -1;
  }
  if(season.season > nextSeason.season) {
    return 1;
  }
  return 0;
}

function loadStart() {
  $('.loading').css('display', 'block');
}

function loadFinish() {
  $('.loading').css('display', 'none');
}

function enableButtons() {
  $('.btn-collapse').prop('disabled', false);
}
